import React from 'react'

import { Route, Routes } from 'react-router-dom'

import Homepage from '../Pages/Homepage'

const Routers: React.FC = () => {
    return (
        <Routes>
            <Route path='/' element={<Homepage />} />
        </Routes>
    )
}
export default Routers