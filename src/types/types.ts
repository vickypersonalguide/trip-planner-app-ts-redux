
export interface Trip {
    id: string;
    destination: string;
    startDate: Date;
    endDate: Date;
    activityTitle: string;
    activityDescription: string;
}

