import React, { useState, useEffect } from 'react'

import { useSelector } from 'react-redux';
import { MdArrowForwardIos } from "react-icons/md"


import Banner from '../components/Banner';
import TripForm from '../components/TripForm'

import { RootState } from '../store/store';
import TripList from '../components/TripList';
import { Trip } from '../types/types';
import { FlexWrapper, StyledCardParent } from "../components/StyledComponent/Styled"

import Jpan from '../Assets/japan.jpg'
import Italy from "../Assets/italy.jpg"
import Zealand from "../Assets/land.jpg"

interface PlanData {
    image: string;
    title: string;
    desc: string;
}

const Homepage: React.FC = () => {

    useEffect(() => {
        const userData = localStorage.getItem("userData");
        if (userData !== null) {
            setIsLoggedIn(JSON.parse(userData));
        }
    }, []);

    const trips: Trip[] = useSelector((state: RootState) => state.trips);

    const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
    const [openModel, setOpenModel] = useState<boolean>(false);

    const handleModel = () => {
        setOpenModel(!openModel)
    }

    const planDatas: PlanData[] = [
        {
            image: Jpan,
            title: 'Japan',
            desc: 'Japan is a fascinating country that seamlessly blends its rich cultural heritage with modern technology. From bustling cities like Tokyo and Osaka to serene traditional villages like Kyoto, there something for every traveler. Explore ancient temples, picturesque gardens, and experience traditional tea ceremonies.',

        },
        {
            image: Italy,
            title: 'Italy',
            desc: 'Italy is a country that captivates visitors with its art, history, and culinary delights. From the romantic canals of Venice to the ancient ruins of Rome and the stunning coastline of the Amalfi Coast, Italy offers diverse landscapes and experiences. ',

        },
        {
            image: Zealand,
            title: 'New Zealand',
            desc: 'For nature lovers and adventure seekers, New Zealand is a dream destination. This island nation is known for its breathtaking landscapes, from majestic mountains and fjords to pristine beaches and lush forests. Explore the stunning Fiordland National Park, experience the geothermal wonders of Rotorua, and visit the vibrant city of Auckland'

        }

    ];
    const editTripId = useSelector((state: RootState) => state.editTrips.editTripId);
    return (
        <div>
            <Banner handleModel={handleModel} />
            <div className="container">
                {trips.length > 0 && isLoggedIn && <h1 className='heading'>Your Plans</h1>}
                {isLoggedIn && <TripList handleModel={handleModel} />}
                <h1 className='heading'>Our Holiday Plans</h1>
                <FlexWrapper>
                    {planDatas.map((data, index) => {
                        return (
                            <StyledCardParent>
                                <img src={data.image} alt='goa trip' />
                                <div>
                                    <h3>{data.title}</h3>
                                    <p>{data.desc}</p>
                                    <div >
                                        <span>Explore</span>
                                        <span><MdArrowForwardIos /></span>
                                    </div>
                                </div>
                            </StyledCardParent>
                        )
                    })}
                </FlexWrapper>

            </div>

            {openModel && <TripForm tripId={editTripId} handleModel={handleModel} type={isLoggedIn} />}



        </div>
    )
}

export default Homepage