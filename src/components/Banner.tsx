import React, { useEffect, useState } from 'react'
import { StyledButton } from './StyledComponent/Styled'

interface Props {
    handleModel: () => void;
}
interface UserData {
    name: string;
    email: string;
}
const Banner: React.FC<Props> = ({ handleModel }) => {

    useEffect(() => {
        const storedUserData = localStorage.getItem("userData");
        if (storedUserData) {
            setUserData(JSON.parse(storedUserData));
        }
        console.log(storedUserData);
    }, []);

    const [userData, setUserData] = useState<UserData | null>(null);

    return (
        <div className='bannerBox'>
            <h1 className='bannerText'>Make Your Tour Plan</h1>
            <div className='d-flex gap-4 '>
                {!userData && <StyledButton type='button' onClick={handleModel} >Login</StyledButton>}
                <StyledButton type='button' onClick={handleModel} >Get Started</StyledButton>
            </div>
        </div>
    )
}
export default Banner