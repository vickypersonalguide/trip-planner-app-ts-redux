
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
import { format } from 'date-fns';
import { DayPicker } from 'react-day-picker';
import { AiFillCloseCircle } from "react-icons/ai"

import { Trip } from '../types/types';
import { RootState } from '../store/store';
import { ModelWrapper, ModelContainer, StyledInput, StyledLable, StyledButton } from "./StyledComponent/Styled"
import { clearEditTripId } from "../store/editTripSlice"
import { addTrip, updateTrip } from '../store/tripsSlice';

import FormImage from '../Assets/formside.jpg'

interface TripFormProps {
    tripId: string | null;
    type: Boolean
    handleModel: () => void;
}
interface UserData {
    name: string;
    email: string;
}

const TripForm: React.FC<TripFormProps> = ({ tripId, handleModel, type }) => {

    const dispatch = useDispatch();
    const trips: Trip[] = useSelector((state: RootState) => state.trips);

    useEffect(() => {
        if (tripId) {
            const existingTrip = trips.find((trip) => trip.id === tripId);
            if (existingTrip) {
                setDestination(existingTrip.destination);
                setStartDate(new Date(existingTrip.startDate));
                setEndDate(new Date(existingTrip.endDate));
                setActivityTitle(existingTrip.activityTitle);
                setActivityDescription(existingTrip.activityDescription)
            }
        }
    }, [tripId]);

    const [destination, setDestination] = useState<string>('');
    const [startDate, setStartDate] = useState<Date | undefined>();
    const [endDate, setEndDate] = useState<Date | undefined>();
    const [activityTitle, setActivityTitle] = useState<string>('');
    const [activityDescription, setActivityDescription] = useState<string>('');
    const [startOpen, setStartOpen] = useState(false);
    const [endOpen, setEndOpen] = useState(false);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");



    const defaultEmail = "test@gmail.com";
    const defaultPassword = "123";

    const handleLogin = () => {
        if (!email || !password) {
            toast.error("All Field are required");
        }
        else if (email === defaultEmail && password === defaultPassword) {
            const userData: UserData = {
                name: "Vicky",
                email: email,
            };
            toast.success("Login successfully")
            localStorage.setItem("userData", JSON.stringify(userData));
            window.location.reload()
        } else {
            toast.error("Invalid credentials. Please try again.");
        }
    };

    const handleOpenStartDay = () => {
        setStartOpen(!startOpen)
    }
    const handleOpenEndDay = () => {
        setEndOpen(!endOpen)
    }
    const handleStartDayClick = (day: Date) => {
        setStartDate(day);
        setStartOpen(!startOpen)
    };
    const handleEndDayClick = (day: Date) => {
        setEndDate(day);
        setEndOpen(!endOpen)
    };

    const handleSubmit = () => {
        if (!destination || !startDate || !endDate || !activityTitle || !activityDescription) {
            toast.error("All Field are required");
        }
        else {
            if (!tripId) {
                const newTrip: Trip = {
                    id: uuidv4(),
                    destination,
                    startDate,
                    endDate,
                    activityTitle,
                    activityDescription
                };
                dispatch(addTrip(newTrip));
                setDestination('');
                setStartDate(undefined);
                setEndDate(undefined);
                setActivityTitle('');
                setActivityDescription('')
                handleModel()
                toast.success("Plan created Successfully")
            } else {
                const updatedTrip: Trip = {
                    id: tripId,
                    destination,
                    startDate,
                    endDate,
                    activityTitle,
                    activityDescription
                };
                dispatch(updateTrip(updatedTrip));
                dispatch(clearEditTripId());
                setDestination('');
                setStartDate(undefined);
                setEndDate(undefined);
                setActivityTitle('');
                setActivityDescription('')
                handleModel()
                toast.success("Plan Updated")
            }
        }
    };
    return (

        <ModelWrapper>
            <ModelContainer>
                <img src={FormImage} alt="PlanSideImage" className='modelImage' />
                <div className='formContainer'>

                    <AiFillCloseCircle className='closeBtn' onClick={handleModel} />
                    <h2> {type ? "Create a New Trip" : "login"}</h2>
                    {type ?
                        <form>
                            <StyledLable> Destination: </StyledLable>
                            <StyledInput type="text" value={destination} onChange={(e) => setDestination(e.target.value)} />
                            <div className='d-flex w-md-75 w-100 justify-content-between'>
                                <div>
                                    <StyledLable>Start Date: </StyledLable>
                                    <p onClick={handleOpenStartDay}>{startDate ? format(startDate, 'PP') : 'No date selected'}.</p>
                                    {startOpen && <DayPicker
                                        mode="single"
                                        disabled={{ before: new Date() }}
                                        className='dayPicker'
                                        onDayClick={handleStartDayClick}
                                    />
                                    }
                                </div>
                                <div>
                                    <StyledLable>End Date:</StyledLable>
                                    <p onClick={handleOpenEndDay}>{endDate ? format(endDate, 'PP') : 'No date selected'}</p>
                                    {endOpen && <DayPicker
                                        mode="single"
                                        className='dayPicker'
                                        disabled={{ before: new Date(), }}
                                        onDayClick={handleEndDayClick}
                                    />}
                                </div>
                            </div>

                            <StyledLable> Activity Title: </StyledLable>
                            <StyledInput
                                type="text"
                                value={activityTitle}
                                onChange={(e) => setActivityTitle(e.target.value)}
                            />

                            <StyledLable> Activity Description:</StyledLable>
                            <StyledInput
                                type="text"
                                value={activityDescription}
                                onChange={(e) => setActivityDescription(e.target.value)}
                            />

                            <StyledButton type="button" onClick={handleSubmit} className='tripBtn'>
                                {tripId ? 'Update Trip' : 'Create Trip and Activities'}
                            </StyledButton>
                        </form> :

                        <div>
                            <StyledLable>Email</StyledLable>
                            <StyledInput
                                type="email"
                                placeholder="Email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />

                            <StyledLable>Password</StyledLable>
                            <StyledInput
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />

                            <StyledButton onClick={handleLogin}>Login</StyledButton>
                        </div>}

                </div>
            </ModelContainer>
        </ModelWrapper>

    );
};
export default TripForm;
