import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { BsCalendarCheck } from "react-icons/bs"
import { BiSolidMap } from "react-icons/bi"

import { setEditTripId, clearEditTripId } from "../store/editTripSlice"
import { RootState } from '../store/store';
import { Trip } from '../types/types';
import { deleteTrip } from '../store/tripsSlice';
import { FlexWrapper, ListContainer, StyledLable, FlexNormal, StyledButton } from './StyledComponent/Styled';
import { toast } from 'react-toastify';

interface Props {
    handleModel: () => void;
}

const TripList: React.FC<Props> = ({ handleModel }) => {
    const dispatch = useDispatch();

    const trips: Trip[] = useSelector((state: RootState) => state.trips);

    const handleDelete = (id: string) => {
        dispatch(deleteTrip(id));
        dispatch(clearEditTripId());
        toast.success("Deleted successfully")
    }
    const handleEdit = (id: string) => {
        dispatch(setEditTripId(id));
        handleModel()
    }
    return (
        <div>
            {trips && <FlexWrapper>
                {trips.map((trip: Trip) => (
                    <ListContainer key={trip.id}>
                        <FlexNormal>
                            <BiSolidMap className='mapIcon' />
                            <h5 className='mb-0 s'>{trip.destination}</h5>
                        </FlexNormal>

                        <FlexNormal>
                            <BsCalendarCheck className='calenderIcon' />
                            <p className='mb-0'>{trip.startDate.toString().slice(0, 10)}</p>
                        </FlexNormal>

                        <FlexNormal>
                            <BsCalendarCheck className='calenderIcon' />
                            <p className='mb-0'>{trip.endDate.toString().slice(0, 10)}</p>
                        </FlexNormal>

                        <p className='mb-1 wrapText'><StyledLable>ActivityTitle</StyledLable> {trip.activityTitle}</p>
                        <p className='mb-1 wrapText'><StyledLable>ActivityDescribtion</StyledLable> {trip.activityDescription}</p>
                        <FlexNormal className='mt-3'>
                            <StyledButton onClick={() => handleDelete(trip.id)} color='#EB3349' color2='#F45C43' left='20px' right='20px' width='100px'>Delete</StyledButton>
                            <StyledButton onClick={() => handleEdit(trip.id)} angle='90deg' color1='#1D976C' color2='#93F9B9' left='20px' right='20px' width='100px'>Edit</StyledButton>
                        </FlexNormal>

                    </ListContainer>
                ))}
            </FlexWrapper>}
        </div>
    );
};

export default TripList;
