import React from 'react'
import { AiOutlineCopyright } from "react-icons/ai"
const Footer: React.FC = () => {
    return (
        <div className='py-3 text-center'><AiOutlineCopyright />CopyRight 2023</div>
    )
}

export default Footer