import React, { useEffect, useRef, useState } from 'react';

import { NavLink } from 'react-router-dom';
import { BiMenu } from 'react-icons/bi';
import { AiOutlineLogout } from "react-icons/ai"
import { IoClose } from 'react-icons/io5';

import { LogoImage, StyledButton, StyledLable } from './StyledComponent/Styled';

import Logo from "../Assets/Trip Planner (1).png"



interface NavLinkItem {
    path: string;
    display: string;
}
interface UserData {
    name: string;
    email: string;
}


const navLinks: NavLinkItem[] = [
    {
        path: '/',
        display: 'Home',
    },
];

const Header: React.FC = () => {

    useEffect(() => {
        const storedUserData = localStorage.getItem("userData");
        if (storedUserData) {
            setUserData(JSON.parse(storedUserData));
        }
        console.log(storedUserData);
    }, []);

    const [userData, setUserData] = useState<UserData | null>(null);
    const menuRef = useRef<HTMLDivElement>(null);

    const toggleMenu = () => {
        if (menuRef.current) {
            menuRef.current.classList.toggle('show__menu');
        }
    };
    const Logout = () => {
        localStorage.removeItem("userData");
        window.location.reload();
    }
    return (
        <header className="d-flex align-items-center fixed-top p-1 header" >
            <div className="container">
                <div className="d-flex align-items-center justify-content-between">
                    <LogoImage src={Logo} alt='logo' />
                    <div className="navigation" ref={menuRef} >
                        <div className='d-flex align-items-center d-block d-md-none justify-content-between mobileShadow '>
                            <div className="align-items-center d-flex gap-2 d-md-none ">
                                {userData ? <><StyledLable className='mb-0'> Hi! {userData.name}</StyledLable>  <StyledButton onClick={Logout} >  <AiOutlineLogout /></StyledButton></> : <StyledLable className='mb-0'> Hi! User</StyledLable>}
                            </div>
                            <span className="d-md-none d-block toggleIcon" onClick={toggleMenu}>
                                <IoClose className="cursor-pointer" />
                            </span>
                        </div>
                        <ul className="menu d-flex align-items-md-center align-items-start flex-column flex-md-row w-100 gap-md-5 gap-3 mb-0 mt-4 mt-md-0 list-unstyled">
                            {navLinks.map((link, index) => (
                                <li key={index} onClick={toggleMenu}>
                                    <NavLink
                                        to={link.path}
                                        className={(navClass) =>
                                            navClass.isActive
                                                ? 'navActive text-decoration-none'
                                                : 'smallText text-decoration-none'
                                        }
                                    >
                                        {link.display}
                                    </NavLink>
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="align-items-center d-none d-md-flex gap-3">
                        {userData ?
                            <>
                                <StyledLable className='mb-0'> Hi! {userData.name}</StyledLable>
                                <StyledButton onClick={Logout} >  <AiOutlineLogout /></StyledButton>
                            </> :
                            <StyledLable className='mb-0'> Hi! User</StyledLable>
                        }
                    </div>
                    <span className="d-md-none d-block toggleIcon" onClick={toggleMenu}>
                        <BiMenu className="cursor-pointer" />
                    </span>
                </div>
            </div>
        </header>
    );
};
export default Header;
