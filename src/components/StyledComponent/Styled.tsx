// styles.tsx
import styled from 'styled-components';
interface GradientButtonProps {
  angle?: string;
  color1?: string;
  color2?: string;
  top?: string;
  bottom?: string;
  left?: string;
  right?: string;
  width?: string;
  display?: string;
}
export const StyledButton = styled.button<GradientButtonProps>`
border: none;
background: linear-gradient(
  ${props => props.angle || '90deg'},
  ${props => props.color1 || '#53b2fe'},
  ${props => props.color2 || '#065af3'}
);
color: #fff;
display: ${props => props.display || 'inline-block'};
width:${props => props.width || ''};
padding: ${props => props.top || '7px'}
${props => props.right || '15px'}
${props => props.bottom || '7px'}
${props => props.left || '15px'};
border-radius: 4px;
 cursor: pointer;

  &:hover {
    background-color: #0056b3;
  }
`;
export const StyledCardParent = styled.div`
width:264px;
height:auto;
border-radius:20px;
overflow:hidden;
display:flex;
background-color:#fff;
flex-direction:column;
margin-bottom:30px;
box-shadow:0.1rem 0.1rem 1rem 0.1rem rgba(0,0,0,.08);

&:hover {
  box-shadow:0.1rem 0.1rem 1.4rem 0.1rem rgba(0,0,0,.24);
}
  > img {
    width:100%;
    height:180px;
    object-fit:cover;
    border-bottom-left-radius:30px;
  
  }
  >div{
     padding:23px;
     
  }
  >div>h3{
    font-size:17px;
    
  }
  >div>p{
    font-size:14px;
    display: -webkit-box;
    -webkit-line-clamp: 3; 
    -webkit-box-orient: vertical;
    overflow:hidden;
  }
  >div>div{
    display: flex;
    justify-content: space-between;
    border-top: 1px solid #ccc;
    padding-top: 15px;
  }
  >div>div>span{
    display:block;
    color: #6872f1;
    font-size:14px;
    font-weight:600;
  }
  
`;
export const LogoImage = styled.img`
width:250px;
height:auto;
object-fit:cover;`;
export const StyledLable = styled.label`
font-size: 17px;
font-weight: 500;
color: #000;
display: block;
margin-bottom: 9px;`;
export const StyledInput = styled.input`
display: block;
padding: 10px 15px;
width: 100%;
border: 1px solid #ccc;
border-radius: 4px;
margin-bottom: 1rem;
`;
export const FlexWrapper = styled.div`
display:flex;
flex-wrap:wrap;
justify-content:center;
alignp-items:center;
gap:35px;
`;
export const ModelWrapper = styled.div`
position: fixed;
width: 100%;
top: 0;
left: 0;
padding:10px;
height: 100vh;
z-index: 1031;
display: flex;
justify-content: center;
align-items: center;
background-color: rgba(0,0,0,0.7);
`;
export const ModelContainer = styled.div`
width: 100%;
max-width: 800px;
height:500px;
display:flex;
flex-wrap:wrap;
border-radius:22px;
position:relative;
background-color:#fff;
box-shadow:0.1rem 0.1rem 1rem 0.1rem rgba(0,0,0,.08);
>img {
  width: 40%;
  height:100%;
  object-fit:cover;
  border-top-left-radius:22px;
  border-bottom-left-radius:22px;
}
>div{
  padding:20px;
  width:60%;
}
>div>h2{
  font-size: 24px;
  font-weight: 600;
  margin-bottom: 19px;
}
`;
export const ListContainer = styled.div`
width:264px;
height:auto;
border-radius:20px;
overflow:hidden;
padding:20px;
display:flex;
background-color:#fff;
flex-direction:column;
margin-bottom:30px;
box-shadow:0.1rem 0.1rem 1rem 0.1rem rgba(0,0,0,.08);

&:hover {
  box-shadow:0.1rem 0.1rem 1.4rem 0.1rem rgba(0,0,0,.24);
}
>h5{
  font-size:20px;
  
}

`;
export const FlexNormal = styled.div`
display:flex;
gap:15px;
margin-bottom:15px;
align-items:center;`
  ;
