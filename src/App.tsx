import React from 'react';

import 'react-day-picker/dist/style.css';
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.css';

import './Assets/Css/styles.css'
import './Assets/Css/responsive.css'

import Layout from './Layout/Layout';

const App: React.FC = () => {
  return (<Layout />);
};

export default App;
