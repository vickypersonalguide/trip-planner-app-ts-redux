import React from 'react'

import { ToastContainer } from 'react-toastify'


import Header from '../components/Header'
import Routers from '../Routes/Routers'
import Footer from "../components/Footer"

const Layout: React.FC = () => {
    return (
        <>

            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <Header />
            <main>
                <Routers />
            </main>
            <Footer />
        </>
    )
}

export default Layout