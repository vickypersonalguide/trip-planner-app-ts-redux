import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Trip } from '../types/types';
import { loadFromLocalStorage, saveToLocalStorage } from '../utils/localStorageUtils';

const initialState: Trip[] = loadFromLocalStorage('trips') || [];

const tripsSlice = createSlice({
    name: 'trips',
    initialState,
    reducers: {
        addTrip: (state, action: PayloadAction<Trip>) => {
            state.push(action.payload);
            saveToLocalStorage('trips', state);
        },
        updateTrip: (state, action: PayloadAction<Trip>) => {
            const updatedTrip = action.payload;
            const index = state.findIndex((trip) => trip.id === updatedTrip.id);
            if (index !== -1) {
                state[index] = updatedTrip;
                saveToLocalStorage('trips', state);
            }
        },
        deleteTrip: (state, action: PayloadAction<string>) => {
            state = state.filter((trip) => trip.id !== action.payload);
            saveToLocalStorage('trips', state);
            return state;
        },
    },
});
export type RootState = ReturnType<typeof tripsSlice.reducer>;
export const { addTrip, updateTrip, deleteTrip } = tripsSlice.actions;
export default tripsSlice.reducer;
