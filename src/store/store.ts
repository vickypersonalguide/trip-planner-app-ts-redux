import { configureStore } from '@reduxjs/toolkit';
import tripsReducer from './tripsSlice';
import editTripReducer from './editTripSlice';

const store = configureStore({
    reducer: {
        trips: tripsReducer,
        editTrips: editTripReducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
