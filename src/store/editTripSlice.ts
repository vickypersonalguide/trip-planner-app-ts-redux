import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface EditTripState {
    editTripId: string | null;
}

const initialState: EditTripState = {
    editTripId: null,
};

const editTripSlice = createSlice({
    name: 'editTrips',
    initialState,
    reducers: {
        setEditTripId: (state, action: PayloadAction<string>) => {
            state.editTripId = action.payload;
        },
        clearEditTripId: (state) => {
            state.editTripId = null;
        },
    },
});

export const { setEditTripId, clearEditTripId } = editTripSlice.actions;
export default editTripSlice.reducer;
