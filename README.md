# Getting Started with Create Trip Planner React App

Follow below the steps

### Step 1 : `Install node ` -  `https://nodejs.org/en/download`

### Step 2 : `clone project `

### Step 3 : `go to project directory `

### Step 4 : `npm install`

### Step 5 : `npm start`


### Login email & password

email : test@gmail.com
password: 123
## Folder structure
travel-planner-app/
│
├── public/
│   ├── index.html
│   └── favicon.png
│
├── src/
│   ├── Assets/
│   │   ├── Css/
│   │   │   ├── default.css
│   │   │   ├── responsive.css
│   │   │   └── styles.css
│   │   ├── banner1.jpg
│   │   ├── formside.jpg
│   │   ├── italy.jpg
│   │   ├── japan.jpg
│   │   ├── land.jpg
│   │   └── Trip Planner (1).png
│   ├── components/
│   │   ├── StyledComponent/
│   │   │   └── styled.tsx
│   │   ├── Banner.tsx
│   │   ├── Footer.tsx
│   │   ├── Header.tsx
│   │   ├── TripForm.tsx
│   │   └── TripList.tsx
│   │
│   ├── Layout/
│   │   └── layout.tsx
│   │
│   ├── Pages/
│   │   └── Homepage.tsx
│   │
│   ├── Routes/
│   │   └── Routers.tsx
│   │
│   ├── store/
│   │   ├── editTripSlice.ts
│   │   ├── store.ts
│   │   └── tripSlice.ts
│   │
│   ├── types/
│   │   └── types.ts
│   │
│   ├── utils/
│   │  └── localStorageUtils.ts
│   │
│   ├── App.test.tsx
│   ├── App.tsx
│   ├──index.tsx
│   ├── react-app-env.d.ts
│   ├── reportWebVitals.ts
│   └── setupTests.ts
│
├── package-lock.json
├── package.json
├── README.md
└── tsconfig.json


Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

### `Live Preview `

https://64c5448f06b69b089e3f9161--kaleidoscopic-paletas-c233e1.netlify.app/
